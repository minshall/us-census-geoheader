#!/usr/bin/emacs --script
(let
    ((org-confirm-babel-evaluate nil)   ; don't ask if it's okay to run fixups
     (ess-ask-for-ess-directory nil))   ; don't ask for "starting directory"
  (package-initialize)                  ; in case either 'org or 'ess are packages
  (require 'org)
  (require 'ess-r-mode)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((R . t)))
  ;; command-line-args-left: https://www.emacswiki.org/emacs/EmacsScripts
  (mapcar 'org-babel-tangle-file command-line-args-left))
