# so:
#
#1) tangle files into package subdirectory
#
# 2) roxygenize in that subdirectory
#
# 3) R cmd 

PKG = us.census.geoheader
PKGVERS = $(shell grep ^' *'Version: us-census-geoheader.org | sed 's/^ *//' | cut -d' ' -f2)
PKGDIR = ./${PKG}

PKGTARGZ = ${PKG}_${PKGVERS}.tar.gz

# subdirectories of package directory
PKGRDIR = ${PKGDIR}/R
PKGMANDIR = ${PKGDIR}/man
PKGINSTDIR = ${PKGDIR}/inst
PKGINSTDOCDIR = ${PKGINSTDIR}/doc
PKGPRIVATEDIR = ${PKGINSTDIR}/private
PKGVIGNETTESDIR = ${PKGDIR}/vignettes

PKGDIRS = ${PKGDIR} ${PKGRDIR} \
				${PKGINSTDIR} ${PKGPRIVATEDIR} ${PKGINSTDOCDIR} \
				${PKGVIGNETTESDIR}

CSVFILE = us2010sf2_101_col_usrdsc.csv
SF2FILE = sf2.pdf

PKGCSVGZFILE = ${PKGPRIVATEDIR}/${CSVFILE}.gz
PKGPRIVATEFILES = ${PKGCSVGZFILE}

ATOURORG = a-tour.org

# org exports from above
ATOURHTML = ${subst org,html,${ATOURORG}}
ATOURPDF = ${subst org,pdf,${ATOURORG}}

# tangled from ${ATOURORG}
ATOURHTMLASIS = ${ATOURHTML}.asis
ATOURPDFASIS = ${ATOURPDF}.asis

# copied from above
PKGATOURHTML = ${PKGVIGNETTESDIR}/${ATOURHTML}
PKGATOURPDF = ${PKGVIGNETTESDIR}/${ATOURPDF}
# inherit target directory from above
PKGATOURHTMLASIS = ${PKGATOURHTML}.asis
PKGATOURPDFASIS = ${PKGATOURPDF}.asis

PKGVIGNETTEFILES = ${PKGATOURHTML} ${PKGATOURHTMLASIS}

# i'd like to include a PDF file, but i get an error message about the two vignettes "having the same vignette name" when i add these to PKGViGNETTEFILES...
# ${PKGATOURPDF} ${PKGATOURPDFASIS}

PKGINSTFILES = ${PKGVIGNETTEFILES}

PKGIMPORTFILES = ${PKGPRIVATEFILES} ${PKGINSTFILES}

ORGFILE = ${subst .,-,${PKG}}.org

PKGRFILES = ${PKGRDIR}/uscgh.R

# we use the roxygen2 to create documentation
PKGNAMESPACE = ${PKGDIR}/NAMESPACE
PKGMAN = ${PKGMANDIR}/uscgh.2010.decode.Rd ${PKGMANDIR}/uscgh.2010.sf2.geoheader.Rd \
			${PKGMANDIR}/ensure.Rd ${PKGMANDIR}/get.data.Rd ${PKGMANDIR}/get.decode.Rd
ROXYGENFILES = ${PKGNAMESPACE} ${PKGMAN}

PKGTANGLED = ${PKGDIR}/DESCRIPTION ${PKGINSTDIR}/CITATION \
					${PKGDIR}/LICENSE ${PKGRFILES}

ATOURTANGLED = ${ATOURHTMLASIS} ${ATOURPDFASIS}

TANGLEDFILES = ${PKGTANGLED} ${ATOURTANGLED}

PKGFILES = ${PKGTANGLED} ${PKGIMPORTFILES} ${ROXYGENFILES}

all: dirs check build

build: dirs ${PKGTARGZ}

${PKGTARGZ}: ${PKGFILES}
	R CMD build ${PKG}

install: ${PKGTARGZ}
	Rscript -e "install.packages(\"${PKGTARGZ}\")"

tangleorg:
	./dotangle.el ${ORGFILE}

tangleatour:
	./dotangle.el ${ATOURORG}

# 'check' checks the *tar.gz* file:
# https://r.789695.n4.nabble.com/Mysterious-NOTE-when-checking-package-tp4757346p4757348.html
check: ${PKGTARGZ}
	R CMD check ${PKGTARGZ}

# NOTE for "maintainer" can be ignored: https://stackoverflow.com/a/23831508
ascran: ${PKGTARGZ}
	R CMD check --as-cran ${PKGTARGZ}

DIRS = ${PKGDIRS}

dirs:
	@for dir in ${DIRS}; \
	do \
		if [ ! -e $${dir} ]; then mkdir $${dir}; fi \
	done

${ROXYGENFILES}: ${ORGFILE} roxygenize

roxygenize:
	(cd ${PKGDIR}; \
		Rscript -e 'require(roxygen2, quietly=TRUE); roxygenize()')

# batch orgmode export: https://stackoverflow.com/a/22091045
${ATOURHTML}: ${ATOURORG}
	emacs $< --batch -f org-html-export-to-html --kill

${ATOURPDF}: ${ATOURORG}
	emacs $< --batch -f org-latex-export-to-pdf --kill

${PKGATOURHTML}: ${ATOURHTML}
	cp -p $< $@

${PKGATOURPDF}: ${ATOURPDF}
	cp -p $< $@
	Rscript -e 'tools::compactPDF("${PKGATOURPDF}", gs_quality = "ebook")'

${PKGATOURHTMLASIS}: ${ATOURHTMLASIS}
	cp -p $< $@

${PKGATOURPDFASIS}: ${ATOURPDFASIS}
	cp -p $< $@

${PKGTANGLED}: tangleorg ${ORGFILE}

${ATOURTANGLED}:  tangleatour ${ATOURORG}

${PKGCSVGZFILE} : ${CSVFILE}
	gzip -c $< > $@

clean:
	@rm -f ${TANGLEDFILES} ${PKGFILES}
	@rm -f ${ATOURHTML}

distclean: clean
	@rm -f ${PKGTARGZ}
